import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  :root {
    --black: #000000;
    --red: #FC2C38;
    --gray: #3D3D3B;
    --white: #FCFCFC;
  }

  @font-face {
    font-family: 'Honor';
    src: URL('/fonts/Honor.ttf') format('truetype');
  }
`;


export default GlobalStyle;